# terminal

#### install php7 mac

```sh
brew tap homebrew/dupes
brew tap homebrew/versions
brew tap homebrew/homebrew-php
brew install php70
```

# ~/.zshrc

#### atalhos

> alias openmamp="cd /Applications/MAMP/htdocs"

> alias phpserver="php -S localhost:7000"


#### mysql cmd mamp

> alias mysql=/Applications/MAMP/Library/bin/mysql

#### atalho - mysql cmd mamp 

> alias openmysql="mysql -uroot -proot"

#### binario do composer

> export PATH="$HOME/.composer/vendor/bin:$PATH"
